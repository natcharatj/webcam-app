import React, { useState, useRef } from "react";
import { Camera } from "react-camera-pro";

const Webcam = () => {
  const camera = useRef(null);
  const [image, setImage] = useState(null);

  return (
    <div>
      <Camera ref={camera} facingMode="environment" aspectRatio={16 / 9} />
      <button onClick={() => setImage(camera.current.takePhoto())}>
        Take photo
      </button>
      <img src={image} alt="Taken photo" />
    </div>
  );
};

export default Webcam;
