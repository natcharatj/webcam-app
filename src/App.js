import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import IdCard from "./component/id-card";
import Home from "./component/home";
import FaceScan from "./component/face-scan";
import ImageInput from "./views/ImageInput";
import ImageInput2 from "./views/imageInput2";

function App() {
  return (
    <>
      <BrowserRouter>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/idcard">
          <IdCard />
        </Route>
        <Route path="/scan">
          {/* <FaceScan /> */}
          {/* <ImageInput /> */}
          <ImageInput2 />
        </Route>
      </BrowserRouter>
    </>
  );
}

export default App;
