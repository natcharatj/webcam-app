import React, { useState, useEffect } from "react";
import Camera from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import { withRouter } from "react-router-dom";
import { loadModels, getFullFaceDescription } from "../api/face";
import test from "../img/test.jpg";
import test1background from "../img/test1background.jpg";
import test2 from "../img/test2.jpg";
import Webcam from "react-webcam";
import { Typography } from "@material-ui/core";

const ImageInput2 = () => {
  const [image, setImage] = useState({
    imageURL: test,
    fullDesc: null,
    detections: null,
  });

  console.log(`image`, image);

  useEffect(async () => {
    await loadModels();
    await handleImage(image.imageURL);
  }, [image.imageURL]);

  const handleImage = async (image) => {
    await getFullFaceDescription(image).then((fullDesc) => {
      if (!!fullDesc) {
        setImage({
          imageURL: image,
          fullDesc,
          detections: fullDesc.map((fd) => fd.detection),
        });
      }
    });
  };

  let drawBox = null;
  if (!!image.detections) {
    drawBox = image.detections.map((detection, i) => {
      let _H = detection.box.height;
      let _W = detection.box.width;
      let _X = detection.box._x;
      let _Y = detection.box._y;
      return (
        <div key={i}>
          <div
            style={{
              position: "absolute",
              border: "solid",
              borderColor: "blue",
              height: _H,
              width: _W,
              transform: `translate(${_X}px,${_Y}px)`,
            }}
          />
        </div>
      );
    });
  }
  const handleTakePhoto = async (dataUri) => {
    await loadModels();
    await handleImage(dataUri);
    // Do stuff with the photo...
    console.log("takePhoto");
  };

  console.log(`drawBox`, !!drawBox);

  return (
    <div>
      {drawBox.length === 0 ? (
        <Typography>not face</Typography>
      ) : (
        <Typography>face</Typography>
      )}
      {/* <button onClick={() => onClick(image.imageURL)}>snap</button> */}
      <div style={{ position: "relative" }}>
        <div style={{ position: "absolute" }}>
          <img src={image.imageURL} alt="imageURL" />
          <Camera
            onTakePhoto={(dataUri) => {
              handleTakePhoto(dataUri);
            }}
          />
        </div>
        {!!drawBox ? drawBox : null}
      </div>
    </div>
  );
};

export default ImageInput2;
