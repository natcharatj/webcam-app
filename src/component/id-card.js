import React, { useState } from "react";
import Camera, { FACING_MODES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { Button } from "@material-ui/core";
import { useHistory } from "react-router";

const IdCard = () => {
  const history = useHistory();
  const [imge, setImge] = useState(null);
  const [mode, setMode] = useState("หน้า");

  const handleTakePhoto = (dataUri) => {
    setImge(dataUri);
  };
  const onClickBack = () => {
    history.push("/");
  };

  const FrontCamera = () => {
    return (
      <Box display="flex" justifyContent="center">
        <Box
          mt={5}
          style={{
            border: "3px solid red",
            zIndex: 1,
            position: "absolute",
            borderRadius: "20px",
            width: 680,
            height: 415,
          }}
        >
          <Grid container style={{ height: "100%" }}>
            <Grid
              style={{ display: "flex", alignSelf: "flex-end" }}
              item
              sm={6}
              md={6}
            >
              <Box
                ml={3}
                mb={5}
                style={{
                  border: "3px solid red",
                  width: "150px",
                  height: "180px",
                }}
              />
            </Grid>
            <Grid
              style={{ display: "flex", justifyContent: "flex-end" }}
              item
              sm={6}
              md={6}
            >
              <Box
                mt={2}
                mr={2}
                style={{
                  borderRadius: "50%",
                  width: "80px",
                  height: "80px",
                  border: "3px solid red",
                }}
              />
            </Grid>
          </Grid>
        </Box>
        <Box style={{ zIndex: -1, position: "absolute" }}>
          <Camera
            idealResolution={{ width: 640, height: 500 }}
            idealFacingMode={FACING_MODES.ENVIRONMENT}
            isImageMirror={true}
            // isSilentMode={true}
            // isDisplayStartCameraError={true}
            onTakePhoto={(dataUri) => {
              handleTakePhoto(dataUri);
            }}
          />
          {imge && (
            <img
              alt=""
              src={imge}
              style={{
                "-webkit-transform": "scaleX(-1)",
                transform: "scaleX(-1)",
              }}
            />
          )}
        </Box>
      </Box>
    );
  };

  const BackCamera = () => {
    return (
      <Box display="flex" justifyContent="center">
        <Box
          mt={5}
          style={{
            border: "3px solid red",
            zIndex: 1,
            position: "absolute",
            borderRadius: "20px",
            width: 680,
            height: 415,
          }}
        >
          <Grid container style={{ height: "100%" }}>
            <Grid
              style={{ display: "flex", justifyContent: "flex-start" }}
              item
              sm={6}
              md={6}
            >
              <Box
                mt={2}
                ml={2}
                style={{
                  borderRadius: "50%",
                  width: "80px",
                  height: "80px",
                  border: "3px solid red",
                }}
              />
            </Grid>

            <Grid
              width="100%"
              item
              sm={6}
              md={6}
              style={{
                display: "flex",
                justifyContent: "flex-end",
                alignSelf: "flex-end",
              }}
            >
              <Box
                mr={3}
                mb={5}
                style={{
                  border: "3px solid red",
                  width: "150px",
                  height: "180px",
                }}
              />
            </Grid>
          </Grid>
        </Box>
        <Box style={{ zIndex: -1, position: "absolute" }}>
          <Camera
            idealResolution={{ width: 640, height: 500 }}
            idealFacingMode={FACING_MODES.ENVIRONMENT}
            isImageMirror={false}
            // isSilentMode={true}
            // isDisplayStartCameraError={true}
            onTakePhoto={(dataUri) => {
              handleTakePhoto(dataUri);
            }}
          />
          {imge && <img alt="" src={imge} />}
        </Box>
      </Box>
    );
  };

  return (
    <>
      {mode === "หน้า" ? <FrontCamera /> : <BackCamera />}
      <Box style={{ display: "flex" }}>
        <Button
          onClick={() => {
            setMode("หน้า");
          }}
        >
          กล้องหน้า
        </Button>
        <Button
          onClick={() => {
            setMode("หลัง");
          }}
        >
          กล้องหลัง
        </Button>
        <Button onClick={onClickBack}>กลับ</Button>
      </Box>
    </>
  );
};
export default IdCard;
