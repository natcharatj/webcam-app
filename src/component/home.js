import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "@material-ui/core";
const Home = () => {
  const history = useHistory();
  const onClickIdCard = () => {
    history.push("/idcard");
  };
  const onClickScan = () => {
    history.push("/scan");
  };
  return (
    <>
      <Button onClick={onClickScan}>Photo</Button>
      <Button onClick={onClickIdCard}>ID Card</Button>
    </>
  );
};

export default Home;
